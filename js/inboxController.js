var inboxController=new function(){
	var m=2*60*1000;
	var a;
	
	var d=0;
	var b={};
	var h=[];
	var k={};
	var g=false;
	var o=false;
	var j=false;
	var l=null;
	var n=this;
	var i='{"threads": "SELECT thread_id, subject, recipients, updated_time, snippet, snippet_author, message_count, unread FROM thread WHERE folder_id = 0",	  "threadsRecipients": "SELECT uid, name FROM user WHERE uid IN (SELECT recipients FROM #threads)",	  "messages": "SELECT thread_id,message_id, author_id, body, created_time, attachment FROM message WHERE thread_id IN (SELECT thread_id FROM #threads)",	  "msgsRecipients": "SELECT uid, name FROM user WHERE uid IN (SELECT author_id FROM #messages)"}';
	this.load=function(p){
		l=p;dataReaday=false;
		j=false;
		facebookAPI.fetchGraphRequest("fql",function(q,y,w){
			try{
				if(q&&q.data&&q.data.length==4&&q.data[0].fql_result_set&&q.data[1].fql_result_set&&q.data[2].fql_result_set&&q.data[3].fql_result_set&&!y&&!w){
					k=q.data[0].fql_result_set;
					var s=q.data[1].fql_result_set;
					var A=q.data[2].fql_result_set;
					var v=q.data[3].fql_result_set;
					var B={};
					for(var t=0;t<A.length;t++){
						B[A[t].uid]=A[t].name}
					for(var t=0;t<v.length;t++){
						B[v[t].uid]=v[t].name}
					for(var t=0;t<k.length;t++){
						for(var r=0;r<k[t].recipients.length;r++){
							var z=k[t].recipients[r];
							k[t].recipients[r]={};
							k[t].recipients[r].id=z;
							k[t].recipients[r].name=B[z]}
					}
					var x={};
					for(var t=0;t<s.length;t++){
						s[t].author_name=B[s[t].author_id];
						if(!x[s[t].thread_id]){
							x[s[t].thread_id]=[]}
						x[s[t].thread_id].push(s[t])}
					for(var t=0;t<k.length;t++){
						k[t].messages=x[k[t].thread_id]}
					a=setInterval(f,m);
					o=true;
					e(true,k);
					if(l)
					{
						l(true)}}
				else{
					c("load inbox failed : "+facebookUtils.getErrorMessageFromAjaxResponse(q))}
			}
			catch(u){
				c("exception occurred while loading inbox : "+(u.message?u.message:u))}
		},["date_format=r","q="+encodeURIComponent(i)])};
	this.clear=function(p)
	{
		if(a){clearInterval(a);
			a=null}
		o=false;
		if(p){
			h=[]}
		k=null;
		d=0;
		b={}
	};
	this.register=function(p)
	{
		h.push(p);
		if(o){
			return k}
		else{
			return null}};
	this.unregisterAll=function(){
		h=[]};
	this.loadFailed=function(){return j};
	this.isDataReady=function(){return o};
	this.getChangedInboxCounter=function(){return d};
	this.initChangedInboxCounter=function(){d=0;b={}};
	var f=function(){
		if(!mainController.isLoggedIn()){
			return}
		if(a){
			clearInterval(a);
			a=null
		}
		facebookAPI.fetchGraphRequest("fql",function(q){
			try{
				if(q&&q.data){
					
					var C=[];
					var w=0;
					var z={};
					for(var H=0;H<k.length;H++){
						z[k[H].thread_id]=k[H]}
					var D=q.data[0].fql_result_set;
					var O=q.data[1].fql_result_set;
					var p=q.data[2].fql_result_set;
					var A=q.data[3].fql_result_set;
					var L={};
					for(var H=0;H<p.length;H++)
		{
			L[p[H].uid]=p[H].name}
		for(var H=0;H<A.length;H++)
		{
			L[A[H].uid]=A[H].name}
		for(var H=0;H<D.length;H++){
			for(var G=0;G<D[H].recipients.length;G++)
			{
				var x=D[H].recipients[G];
				D[H].recipients[G]={};
				D[H].recipients[G].id=x;
				D[H].recipients[G].name=L[x]}
		}var r={};
		for(var H=0;H<O.length;H++){
			O[H].author_name=L[O[H].author_id];
			if(!r[O[H].thread_id])
			{
				r[O[H].thread_id]=[]}
			r[O[H].thread_id].push(O[H])}
		for(var H=0;H<D.length;H++){
			D[H].messages=r[D[H].thread_id]}
		for(var H=0;H<D.length;H++){
			if(!z[D[H].thread_id]){
				C.push({data:D[H],newThread:true});
				try
				{
					var u="";
					var N;
					var s="";
					var B="";
					var I=true;
					if(D[H].snippet_author){
						B=D[H].snippet_author;
						for(var G=0;G<D[H].recipients.length;G++){
							if(D[H].recipients[G].id==B)
							{
								u=D[H].recipients[G].name}}
					}
					if(D[H].snippet)
					{
						s=D[H].snippet}
					if(D[H].updatedTime)
					{
						N=new Date(D[H].updatedTime*1000)}
					if(D[H].unread&&D[H].unread>0)
					{
						I=false;
						b[D[H].thread_id]=1;
						w++}
					var F=true;
					if(B&&B==mainController.userDataController.getId()){
						F=false
					}
					var t=(new Date()).getTime();
					if(N&&(t-N.getTime()>24*60*60*1000))
					{
						F=false}
					if(I)
					{
						F=false}
					var v=B?Constants.FACEBOOK_GRAPH_API+B+"/picture?type=square":"img/navmenu-inbox.png";
					var y="New inbox message";
					if(u){y=u;
						if(!s){y+=" sent you an inbox message"}
					}
					if(F){
						mainController.showDesktopNotification(v,y,s,"http://www.facebook.com/messages/")
					}
				}
				catch(J){
					console.log("exception occurred in update inbox (new) : "+(J.message?J.message:J))}
			}
			else{
				try{
					var u;
					var B;
					var N;
					var s;
					var I=true;
					if(D[H].snippet&&z[D[H].thread_id]&&z[D[H].thread_id].snippet)
					{
						if(D[H].snippet!=z[D[H].thread_id].snippet)
						{
							C.push({data:D[H],newThread:false});
							var s=D[H].snippet;
							if(D[H].snippet_author)
							{
								B=D[H].snippet_author;
								for(var G=0;G<D[H].recipients.length;G++){
									if(D[H].recipients[G].id==B)
									{
										u=D[H].recipients[G].name}}
							}
							var K=B?Constants.FACEBOOK_GRAPH_API+B+"/picture?type=square":"img/navmenu-inbox.png";
							if(D[H].updated_time){
								N=new Date(D[H].updatedTime*1000)}
							if(D[H].unread&&D[H].unread>0)
							{
								I=false}
							var F=true;
							if(B&&B==mainController.userDataController.getId())
							{F=false}
							var t=(new Date()).getTime();
							if(N&&(t-N.getTime()>24*60*60*1000))
							
							{F=false}
							if(I){
								F=false}
							var y="New inbox message";
							if(u){
								y=u;
								if(!s){
									y+=" sent you an inbox message"}
							}
							if(F){
								mainController.showDesktopNotification(K,y,s,"http://www.facebook.com/messages/")}
							if(!b[D[H].thread_id]&&!I){
								w++;b[D[H].thread_id]=1}
						}
					}
					if(D[H].unread==0&&b[D[H].thread_id])
					{
						w--;delete b[D[H].thread_id]
					}
				}
				catch(J){
					console.log("exception occurred in update inbox (existing) : "+(J.message?J.message:J))
				}
			}
		}
		d+=w;
		k=D;
		mainController.updateBadge();
		var M={};
		M.changedThreads=C;
		M.changedInboxCounter=d;
		e(true,M)
				}
				else
				{
					var E=facebookUtils.getErrorMessageFromAjaxResponse(q);
					console.error("failed load inbox updated : "+E);
					mainController.checkTokenValidation(E)}
			}
			catch(J){
				console.error("error occurred when updating inbox : "+(J.message?J.message:J))}
			if(mainController.isLoggedIn())
			{
				a=setInterval(f,m)}},["date_format=r","q="+encodeURIComponent(i)])};
	var e=function(s,q){
		for(var p=0;p<h.length;p++){
			try{h[p].notify(s,q,"inbox")}
			catch(r){
				console.error("failed notify observer (maybe closed during action) : "+(r.message?r.message:r))}}};
	var c=function(p){
		console.error(p);
		j=true;
		e(false);
		if(l)
		{
			l(false)}n.clear(false)}}();
