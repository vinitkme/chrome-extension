var backgroundPage;
var currentMenuItem="newsFeedTopContainer";
var startTime;
var totalTime;
var dialog=new function(){
	this.close=function(){window.open("","_self");
		window.close()};
	this.showLoginScreen=function(){
		$("#loadingDiv").hide();
		$("#facebookDiv").hide();
		$("#loginDiv").show()};
	this.showFacebookScreen=function(){
		$("#loadingDiv").hide();
		$("#loginDiv").hide()};
	this.showErrorScreen=function(){
		$("#loadingDiv").hide();
		$("#loginDiv").css("display","none");
		$("#rightPane").css("display","none");
		$("#facebookDiv").show();
		$("#facebookDiv").css("opcaity","1");
		$("#problem").show()};
	this.hideErrorScreen=function(){
		$("#problem").css("display","none");
		$("#rightPane").css("display","block");
		$("#facebookDiv").css("display","block")};
	this.registerViews=function(){userDataView.registerView();
		newsFeedView.registerView();
		eventsView.registerView();
		albumsView.registerView();
		inboxView.registerView();
		notificationsView.registerView();
		friendsView.registerView()}}();
		var mouseHoverMenu=false;
		$(function(){onPopupLoad()});
		function closeLoadingWindow(){
			$("#facebookDiv").show();
			$("#loadingDiv").fadeOut("slow")}
			function onPopupLoad(){
				$(document).disableSelection();
				backgroundPage=chrome.extension.getBackgroundPage();
				$(".menuBadge").hide();
				$(".logoutFancy").fancybox({titlePosition:"inside",transitionIn:"fade",transitionOut:"fade",margin:0,padding:0});
				$("#btnLogout").click(function(){$("#logoutQuestion").show()});
				$("#logoutQuestionButtonYes").click(function(){backgroundPage.mainController.doLogout();dialog.close()});
				$("#logoutQuestionButtonNo").click(function(){$.fancybox.close("#logoutQuestion")});
				$("#banner").click(function(){_gaq.push(["_trackEvent","cubiezCampaign","bannerClick"]);
					window.open("http://cubiez.com/FacebookLandingPage.htm").focus()});
				var a={menuItemNewsFeed:"newsFeedTopContainer",menuItemFriends:"friendsTopContainer",menuItemCalendar:"calendarTopContainer",menuItemAlbums:"albumsTopContainer",menuItemNotifications:"notificationsTopContainer",menuItemInbox:"inboxTopContainer"};
				$(".navMenuItem").click(function(){$("#rightPane > *").fadeOut();
					$(".navMenuItem").removeClass("navMenuItemSelected");
					$(this).addClass("navMenuItemSelected");
					totalTime=parseInt(Math.floor((new Date().getTime()-startTime)/1000));
					startTime=new Date().getTime();
					_gaq.push(["_trackEvent","time in item",currentMenuItem,"time in "+currentMenuItem,totalTime]);
					currentMenuItem=a[$(this).attr("id")];
					$("#"+a[$(this).attr("id")]).fadeIn()});
				$("#menuItemNewsFeed").click(function(){_gaq.push(["_trackEvent","menuItem","newsFeed selected"]);
					newsFeedView.switchToMain()});
				$("#menuItemFriends").click(function(){_gaq.push(["_trackEvent","menuItem","friends selected"]);
					friendsView.switchToFriends()});
				$("#menuItemCalendar").click(function(){_gaq.push(["_trackEvent","menuItem","calendar selected"]);
					eventsView.switchToEvents()});
				$("#menuItemAlbums").click(function(){_gaq.push(["_trackEvent","menuItem","albums selected"]);
					albumsView.switchToAlbums()});
				$("#menuItemNotifications").click(function(){_gaq.push(["_trackEvent","menuItem","notifications selected"]);
					notificationsView.updateNotifications()});
				$("#menuItemInbox").click(function(){_gaq.push(["_trackEvent","menuItem","inbox selected"]);
					inboxView.switchToInbox()});
				$("#btnLogin").click(function(){dialog.registerViews();
					backgroundPage.mainController.doLogin()});
				$("#btnReloadNewsFeed").click(function(){$("#newsFeed").hide();
					$("#newsFeedReload").hide();
					$("#newsFeedLoading").show();
					backgroundPage.mainController.newsFeedController.load()});
				$("#btnReloadFriends").click(function(){$("#friends").hide();
					$("#friendsReload").hide();
					$("#friendsStillLoading").show();
					backgroundPage.friendsController.load(function(){backgroundPage.eventsController.load()})});
				$("#btnReloadCalendar").click(function(){$("#calendar").hide();
					$("#calendarReload").hide();
					$("#calendarStillLoading").show();
					backgroundPage.eventsController.load()});
				$("#btnReloadAlbums").click(function(){$("#albums").hide();
					$("#albumsReload").hide();
					$("#albumsStillLoading").show();
					backgroundPage.albumsController.load()});
				$("#btnReloadNotifications").click(function(){$("#notifications").hide();
					$("#notificationsReload").hide();
					$("#notificationsStillLoading").show();
					$("#events").hide();
					$("#eventsReload").hide();
					$("#eventsStillLoading").show();
					backgroundPage.notificationsController.load(function(){backgroundPage.eventsController.load()})});
				$("#btnReloadInbox").click(function(){$("#inbox").hide();
					$("#inboxReload").hide();
					$("#inboxStillLoading").show();
					backgroundPage.inboxController.load()});
				$("#logoutArrow").click(function(){$("#btnLogout").toggle(100);
					var c=$(this).text();
					$(this).text((c=="\u25BC")?"\u25B2":"\u25BC")});
				var b=$('<iframe id="fbLike" src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com/pages/FBChrome/337203696299270&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=131291866988365">');
				$("#donations").before(b);
				checkInternetConnection()}
				function checkInternetConnection(){
					$.ajax({url:"https://www.facebook.com",timeout:Constants.AJAX_TIMEOUT,success:function(a){hasInternetConnection()},error:function(a,c,b){noInternetConnection()}})}
					function hasInternetConnection(){
						if(!backgroundPage.mainController.isLoggedIn())
						{dialog.showLoginScreen()}
						else
						{dialog.showFacebookScreen();
							$("#rightPane").show();
							userDataView.init();
							newsFeedView.init();
							notificationsView.init();
							friendsView.init();
							eventsView.init();
							inboxView.init();
							albumsView.init();
							dialog.registerViews()}}
							function noInternetConnection(){
								dialog.showErrorScreen()}
								$(window).unload(function(){
									totalTime=parseInt(Math.floor((new Date().getTime()-startTime)/1000));
									_gaq.push(["_trackEvent","time in item",currentMenuItem,"time in "+currentMenuItem,totalTime]);
									backgroundPage.mainController.unregisterObservers()});
