var facebookAPI = new function(){
	this.fetchGraphRequest=function(b,e,a){
		if(!e){
			return}
		if(b==undefined)
		{
			b=""
		}

		var d=Constants.FACEBOOK_GRAPH_API+b;
		if(!a){
			a = new Array()
		}

		var c=mainController.getToken();
		if(c){
			a.push("access_token="+c)
		}
		if(a.length>0){
			d+="?"+a.join("&")
		}

		fetchAjaxRequest(d,e)
	};

	fetchAjaxRequest=function(a,b){
		if(!b){
			return}
		$.ajax({dataType:"jsonp",url:a,timeout:Constants.AJAX_TIMEOUT,success:function(c){
			try{
				b(c)}
			catch(d){
				console.log("Check")}},error:function(c,g,f){
					try{
						b(c)
					}
					catch(d){}}})}}();
