var friendsView = new function(){
	var e=false;
	var k=this;
	var d=false;
	var f=null;
	var r=false;
	var n=true;
	var p=null;

	this.init=function(){
		if(e){
			return
		}
		e=true;
		l();
		a();
		k.reload()
	};
	this.registerView=function(){
		try{
			if(d){
				return
			}
			f=backgroundPage.friendsController;
			d=true;
			p=f.register(k);
			if(f.isDataReady()&&p){
				r=true;
				k.onFriendLoaded(p);
				if(currrentMenuItem=="friendsTopContainer"){
					o()}
				else
				{
					$("#friends").hide();
					$("#friendsStillLoading").show()}
				else{
					if(f.loadFailed(){$("#friends").hide();
						$("#friendsReload").show()
					}
					else
					{
						$("#friends").hide();
						$("#friendsStillLoading").show()}
						}
						}
						catch(s){
							console.error("EXE | friendsView :"+(s.message?s.message:s))}
							};
							this.notify=function(v,t,s){

								try{
		if(v){
			if(!r){
				r=true;
				p=t;
				k.onFriendsLoaded(t);
				if(currentMenuItem="friendsTopContainer")
				{
					o()}
				else{
					$("#friends").hide();
					$("#friendsStillLoading").show()}
			}
			else{
				$("#friendsStillLoading").hide();
				$("#friendsReload").show()}
		}
		catch(u){
			console.error("EXE | friendsView.notify:"+(u.message?u.message:u))}
								};
								this.clear=function(){
d=false;
                f=null;
                r=false;
                p=null;
                n=true
        };
        this.reload=function(){
                $("#friendsList").empty();
                $("#friendsErrorMessage").hide();
                $("#friendsLoading").show()}
        ;
        this.switchToFriends=function(){
                if(n&&p){
                        n=false;
                        b(p);
                        $("#friendsStillLoading").hide();
                        $("#friends").fadeIn()}}
        ;this.onFriendsLoaded=function(s){
                try{
                        if(!e){
                                k.init()}
                        $("#friendsLoading").hide();
                        if(!s){g();
                                return
                        }
                        if(!s.length){
                                q();
                                return}
                }
                catch(t){
                        console.error("EXE | friendsView.onFriendsLoaded : "+(t.message?t.message:t))}
        };

	this.onUpdate=function(v,t){
	try{
		if(!v||!t){
		console.error("ERR |dialog_friends | onUpdate | data is not valid");
			return }
		for(var s=0;s<v.length;s++)
{		
		c(v[s])
}

	for(var s=0;s<t.length;s++)
	{
			h(t[s])
	}
		catch(u){
			 console.error("EXE | friendsView.onUpdate : "+(u.message?u.message:u))}
						        };
		
	var o=function(){
		n=false;
		b(p);
		$("#friendsStillLoading").hide();
		$("#friends").fadeIn()};
	var i=function(t){
		try{
			if(!t||!t.id||!t.name){
				return}
			var u="http://graph.facebook.com/"+t.id+"/picture?type=square";backgroundPage.mainController.cacheImage(u);
			var s='<div id="@@@FRIEND_ID@@@" class="friendContainer">                                                       <div class="friendImgWrapper">                                                          <a href="@@@FRIEND_LINK@@@" class="friendImgLink ga_friend_link" target="_blank"><img src=@@@FRIEND_IMAGE@@@ alt="">                                                                    </a>                                                    </div>                                                  <a href="@@@FRIEND_LINK@@@" class="friendNameLink ga_friend_link" target="_blank">                                                              @@@FRIEND_NAME@@@                                                       </a> </div>';
			s=s.replace(/@@@FRIEND_ID@@@/g,t.id);
			s=s.replace(/@@@FRIEND_NAME@@@/g,t.name);
			s=s.replace(/@@@FRIEND_LINK@@@/g,Constants.FACEBOOK_URL+t.id);
			s=s.replace(/@@@FRIEND_IMAGE@@@/g,u);
			return $(s)}

		catch(v)
		{
			console.error("EXE | friendsView.createFrienContainerDiv:"+(v.message?v.message:v));
			return}
	};

	var c=function(t){
		try{
			friendsDiv=i(t);
			var v=$("#friendsList");
			var s=false;
			$(".friendNameLink").each(function(x){
				if(t.name<$(this).text().trim()){
					s=true;
					var w=$(this).closest(".friendContainer");
					$(w).before(friendDiv);
					return false}
			});
			if(s==false){
				$(".friendContainer:last").after(friendDiv)}
		}
		catch(u){
			console.error("EXE | friendsView.addFriend :"+(u.message?u.message:u))}
	};

	 var h=function(s){
                $(".friendNameLink:contains('"+s.name.toString()+"')").closest(".friendContainer").remove()};
        var b=function(t){
                try{
                        var u=new Array();
                        for(var s=0;s<t.length;++s){
                                u.push(t[s])}
                        t=u;
                        t.sort(function(y,x){
                                if(y.name<x.name){
                                        return -1}
                                if(y.name>x.name){
                                        return 1
                                }
                                return 0});
                        var w=$("#friendsList");
                        for(var s=0;s<t.length;++s){
                                w.append(i(t[s]))}
                        w.append($('<div id="friendsListFooter"></div>'));
                        j($("#txtFriendsFilter").val())}
                catch(v){
                        console.error("EXE | friendsView.populateFriendsList : "+(v.message?v.message:v))}};

	var m=function(){function s(x){
		if(!x||!x.length){
			return}
		var t=x.attr("id");
		if(!t){
			return}
		var v=$('<img src="http://graph.facebook.com/'+t+'/picture?type=square" alt=""/>');
		var u=null;
		var w=null;

		$(".imgFriendLoading").each(function(y){
                        var z=$(this).is(":visible");
                        if(z){
                                u=$(this).closest(".friendContainer");
                                return false}
                        else{
                                if(!w){
                                        w=$(this).closest(".friendContainer")}
                        }
                });
                if(!u){
                        u=w}
                v.load(function(){
                        s(u)});
                v.error(function(){
                        s(u)});
                x.find(".friendImgLink").html(v)
        }
        s($(".imgFriendLoading:first").closest(".friendContainer"))};
        var g=function(s){
                var t=$("#friendsErrorMessage");
                t.find("img").attr("src","img/friends.png");
                t.find("span").text("Unable to view your friends.");
                t.show()};
        var q=function(s){
                var t=$("#friendsErrorMessage");
                t.find("img").attr("src","img/forever_alone.jpg");
                t.find("span").text("You have no friends.");
                t.show()};
	

	var l=function(){
		$(".ga_friends_link").live("click",function(){
			  _gaq.push(["_trackEvent","exit","friend"])});
                $(".friendContainer").live({
                        mouseenter:function(){
                                $(this).find(".friendNameLink").css(
                                        {
                                                "text-decoration":"underline"})
                        },mouseleave:function(){
                                $(this).find(".friendNameLink").css(
                                        {"text-decoration":"none"})
                        }
                })
        };
        var a=function(){
                var s="";
                $("#txtFriendsFilter").keydown(function(t){
                        if(t.keyCode=="13"){
                                t.preventDefault()}
                }
                );
                $("#txtFriendsFilter").keyup(function(t){
                        var u=$(this).val();
                        if(s==u){
                                return}
                        s=u;
                        j(u)});
                $("#txtFriendsFilter").blur(function()
                                {
                                        var t=$(this).val();if(!t){
                                                s="";
                                                $(this).text("");
                                                $(".friendContainer").show()}
                                })};
                var j=function(s){
                        try{
                                if(!s){
                                        $(".friendContainer").show();return
                                }
                                $(".friendNameLink").each(function(u){
                                        var v=$(this).text();
                                        if(v.toUpperCase().indexOf(s.toUpperCase())>=0){
                                                $(this).parent().show()
			 }
					                                        else{
											                                                $(this).parent().hide()}
				                                }
								                                )}
				                                catch(t){
									                                        console.error("EXE | friendsView.filterFriends : "+(t.message?t.message:t))}
								                }
}();

