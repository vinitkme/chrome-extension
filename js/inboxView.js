var inboxView=new function(){
	var c;
	var q=null;
	var n=null;
	var r={};
	var g=false;
	var m=this;
	this.init=function(){
		$(".thread").live("click",function(){
			var u=$(this).attr("id").replace("thread_","");
			var t=r[u];
			f(t)});
		$("#btnThreadMessagesBack").click(function(){
			$("#inboxThread").show();
			$("#threadMessages").animate({left:"100%"},k,function(){
				$("#threadMessages").hide()})});
		$("#btnThreadMessagesReply").click(function(){
			w=window.open("http://www.facebook.com/messages/?action=read&tid=id."+c).focus()});
		$("#threadMessageContent").scroll(function(){
			return;
			if(i||h){
				return
			}
			var t=($(this)[0].scrollHeight-$(this)[0].clientHeight)-$(this).scrollTop();
			if(t<800){
				console.log("scrolling...");
				if(p[c].moreMessagesURL){
					h=true;
					$("#threadMessageContent").append('<img class="currentLoader" src="img/fb_loader.gif" width=16px height=11px><img>');
					d(p[c].moreMessagesURL)
				}
				else{
					i=true;return}}});
		$("#inboxSeeMoreMessages a").live("click",function(){
			window.open(Constants.FACEBOOK_URL+"messages").focus()});
		$("#inboxSeeMoreThreadMessages a").live("click",function(){
			window.open("http://www.facebook.com/messages/?action=read&tid=id."+c).focus()})};
		this.registerView=function(){
			try{
				if(g){
					return
				}
				g=true;
				q=backgroundPage.inboxController;
				n=q.register(m);
				if(q.isDataReady()&&n){
					console.log("onInboxLoaded from init");
					m.onInboxLoaded(n)}
				else{
					if(q.loadFailed()){
						$("#inbox").hide();
						$("#inboxReload").show()}
					else{
						$("#inbox").hide();
						$("#inboxStillLoading").show()}}
			}
			catch(t)
			{
							console.error("EXE | inboxView.registerView : "+(t.message?t.message:t))}
		};
		this.notify=function(x,u,t){
			try{
				if(x){
					if(!n){
						console.log("onInboxLoaded from notify");
						m.onInboxLoaded(u);
						$("#inboxStillLoading").hide();
						$("#inbox").fadeIn()}
					else{
						m.onUpdate(u.changedThreads,u.changedInboxCounter)}
				}
				else{
					$("#inboxStillLoading").hide();
					$("#inboxReload").show()}
			}
			catch(v){
				console.error("EXE | inboxView.notify : "+(v.message?v.message:v))}
		};
		this.clear=function(){
			q=null;
			n=null;
			g=false};
		this.onInboxLoaded=function(u){
			try{
				n=u;
				for(var t=0;t<n.length;t++){
					e[n[t].thread_id]=n[t].updated_time;
					$("#inboxThread").append(l(n[t]));
					r[n[t].thread_id]=n[t]}
				$("#inboxThread").append(b());
				setInterval(function(){
					$(".thread").each(function(){
						var x=e[$(this).attr("id").replace("thread_","")];
						$(this).find(".threadTime").text(DateUtil.formatDateTimeForFeedDisplay(new Date(x*1000)))})},60*1000);
				this.updateInboxBadge()
			}
			catch(v)
			{
				console.error("EXE | inboxView.onInboxLoaded : "+(v.message?v.message:v))}
		};
		this.updateInboxBadge=function(){
			try{
				var u=backgroundPage.inboxController.getChangedInboxCounter();
				if(u){
					if(u<=99){
						$("#inboxBadge").text(u)
					}
					else{
						$("#inboxBadge").text("99+")}
					$("#inboxBadge").show()}
				else{
					$("#inboxBadge").hide()}
			}
			catch(t){
				console.error("EXE | inboxView.updateInboxBadge : "+(t.message?t.message:t))}
		};
		this.onUpdate=function(u,y){
			try{
				var t;
				m.updateInboxBadge();
				for(var v=0;v<u.length;v++){
					r[u[v].data.thread_id]=u[v].data;
					e[u[v].data.thread_id]=u[v].data.updated_time;
					if(u[v].newThread==false){
						t="thread_"+u[v].data.thread_id;
						$("#"+t).next().remove();
						$("#"+t).remove();
						$("#inboxThread").prepend(l(u[v].data))}
					else{
						$("#inboxThread").prepend(l(u[v].data))}
					if($("#inboxThread").is(":visible")){
						m.switchToInbox()}}
			}
			catch(x){
				console.error("EXC | inboxView.onUpdate : "+(x.message?x.message:x))}
		};
		this.switchToInbox=function(){
			try{
				$("#inboxWrapper").show();
				var u=backgroundPage.inboxController.getChangedInboxCounter();
				backgroundPage.inboxController.initChangedInboxCounter();
				backgroundPage.mainController.updateBadge();
				$("#inboxBadge").hide();
				$(".thread").each(function(){
					var v=e[$(this).attr("id").replace("thread_","")];
					$(this).find(".threadTime").text(DateUtil.formatDateTimeForFeedDisplay(new Date(v*1000)))});
				$(".thread:lt("+u+")").hide();
				$(".thread:lt("+u+")").css("background-color","#FFFBDD");
				$(".thread:lt("+u+")").slideDown("300",function(){
					$(this).delay(1500).animate({backgroundColor:"#FFFFFF"},1500,function(){
						$(this).attr("style","")})});
				if($("#threadMessages").is(":visible")){
					$("#inboxThread").show();
					$("#threadMessages").animate({
						left:"100%"},k,function(){
							$("#threadMessages").hide()})}
			}
			catch(t){
				console.error("EXC | inboxView.switchToInbox : "+(t.message?t.message:t))
			}
		}
		;
		var k=300;
		var n;
		var p=new Object();
		var e=new Object();
		var i=false;
		var c;
		var h=false;
		var o='<div class="thread" id="thread_@@@threadId@@@"><div class="threadLeftPane"><img class="threadPhoto" src="@@@threadPhoto@@@" alt="threadPhoto"/></div><div class="threadRightPane"><div class="threadTime">@@@threadUpdateTime@@@</div><div class="threadMembers">@@@threadMembers@@@</div><div class="threadLastMessage">@@@threadLastMessage@@@</div></div></div><hr/>';
		var s='<div class="threadMessage"><div class="threadMessageLeftPane"><img class="messagePhoto" src="@@@messagePhoto@@@" alt="no message photo"/></div><div class="threadMessageRightPane"><div class="messageTime">@@@messageTime@@@</div><a class="messageSender" href=@@@senderLink@@@ target="_blank">@@@messageSender@@@</a><div class="messageText">@@@messageText@@@</div></div></div><hr/>';
		var l=function(B){
			try{
				var C=B.snippet_author;
				var y=backgroundPage.mainController.userDataController.getId();
				if(C==y){
					for(var x=B.messages.length-1;x>=0;x--){
						if(B.messages[x].author_id!=y){
							C=B.messages[x].author_id;break}
					}
				}
				var D=Constants.FACEBOOK_GRAPH_API+C+"/picture?type=square";
				var t=B.snippet;
				var A=DateUtil.formatDateTimeForFeedDisplay(new Date(B.updated_time*1000));
				var v="";
				for(var x=0;x<B.recipients.length&&x<Constants.MAX_MEMBERS_SHOWN_IN_THREAD;x++){
					if(B.recipients[x]&&B.recipients[x].id!=y){
						v+=B.recipients[x].name+", "}
				}
				if(B.recipients.length>Constants.MAX_MEMBERS_SHOWN_IN_THREAD){
					v+=(B.recipients.length-Constants.MAX_MEMBERS_SHOWN_IN_THREAD)+" others"
				}
				else{
					v=v.substring(0,v.length-2)
				}
				var u=o.replace("@@@threadId@@@",B.thread_id).replace("@@@threadPhoto@@@",D).replace("@@@threadUpdateTime@@@",A).replace("@@@threadMembers@@@",v).replace("@@@threadLastMessage@@@",Utils.nl2br(t,true));
				return u
			}
			catch(z)
			{
				console.error("EXC | inboxView.createThreadDiv : "+(z.message?z.message:z));
				return
			}
		};
		var j=function(x){
			try{
				var v=x.author_name;
				var t=Constants.FACEBOOK_GRAPH_API+x.author_id+"/picture?type=square";
				var z="";
				if(x.created_time){
					z=DateUtil.formatDateTimeForFeedDisplay(new Date(x.created_time*1000))
				}
				messageText=x.body;
				var u=s.replace("@@@messagePhoto@@@",t).replace("@@@messageTime@@@",z).replace("@@@messageSender@@@",v).replace("@@@senderLink@@@","http://www.facebook.com/profile.php?id="+x.author_id).replace("@@@messageText@@@",Utils.nl2br(messageText,true));
				return u}catch(y){
					console.error("EXC | inboxView.createMessageDiv : "+(y.message?y.message:y));
					return}
		};
		var f=function(t){
			try{
				i=false;
				h=false;
				$("#threadMessageContent").empty();
				c=t.thread_id;
				for(var v=0;v<t.messages.length;v++){
					$("#threadMessageContent").append(j(t.messages[v]))}
				$("#threadMessages").show();
				var u=$("#threadMessageContent")[0].scrollHeight-$("#threadMessageContent").height();
				$("#threadMessageContent").animate({
					scrollTop:u
				},"slow");$("#threadMessages").animate({left:"0%"},k,function(){
					$("#inboxThread").hide()})
			}
			catch(x){
				console.error("EXC | inboxView.createMessageDiv : "+(x.message?x.message:x))}
		};
		var d=function(t){
			$.ajax({
				url:t,data:"access_token="+backgroundPage.mainController.getToken(),type:"POST",dataType:"jsonp",headers:{"Content-type":"application/x-www-form-urlencoded","Content-length":1},success:function(v){
					var x=$("#threadMessageContent > .threadMessage:last").find(".messageText").text();
					if(v&&v.data&&v.data.length&&v.data[0].message!=x){
						x=v.data[0].message;
						var u=v.data[0];
						var y=j(u);
						$(".currentLoader").remove();
						$("#threadMessageContent").append(y);
						if(v.paging&&v.paging.next){
							p[c].moreMessagesURL=v.paging.next
						}
						else{
							i=true}
					}
					else{
						$(".currentLoader").remove();
						if(p[c].first_message)
			{
				$("#threadMessageContent").append(j(p[c].first_message))}
			i=true
					}
					h=false
				},error:function(u,x,v){
					console.error("EXC | load extra message failed : "+x+" , "+v);
					h=false}
			})};
		var b=function(){
			var t='<div id="inboxSeeMoreMessages" class="feedbackBox boxLoading">                                   <a>See more...</a>                                </div>';
			return t};
		var a=function(){
			var t='<div id="inboxSeeMoreThreadMessages" class="feedbackBox boxLoading">                                   <a>See more...</a>                                </div>';
			return t}
}();
