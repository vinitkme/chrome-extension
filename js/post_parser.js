var postParser=function(b)
{
	this.getTitle=function(){
		var h=b.from.name;
		if(b.to&&b.to.data&&b.to.data.length==1){
			try{
				var g=true;
				var d=b.to.data[0].id;
				if(b.message_tags){
					for(var c in b.message_tags){
						if(b.message_tags[c][0].id==d)
						{
							g=false;break}}}
				if(g)
				{h+=" \u25B6 "+b.to.data[0].name}}
			catch(f){
				console.error("Error in PostParser.getTitle : "+f.description?f.description:f)}}
		return h};
	this.getPublishDate=function(){
		return new Date(b.created_time)};
	this.getProfileLink=function(){
		return"http://www.facebook.com/"+b.from.id};
	this.getProfileImage=function(){
		return"http://graph.facebook.com/"+b.from.id+"/picture/?type=square"};
	this.getMessage=function(){
		var c;
		if(b.message){c=b.message}else{if(b.story){c=b.story}}
		if(!c){c=""}
		if(c.search(/(<|>)/)!=-1){
			c=$("<pre>").text(c).html()}
		c=Utils.replaceURLWithHTMLLinks(c);
		c=Utils.nl2br(c,true);
		if(b.message_tags){
			c=a(c,b.message_tags)}
		if(b.story_tags)
		{c=a(c,b.story_tags)}
		return c};
	this.getPostLink=function(){
		var c;
		if(b.link){
			c=b.link}
		else{
			if(b.source){
				c=b.source}}return c};
	this.getPostLinkCaption=function(){var c;
		if(b.name){c=b.name}
		
		else
		{if(b.caption){c=b.caption}else{c="("+b.type+")"}}return c};
	this.countLikes=function(){if(!b.likes){return 0}
		if(b.likes.count==undefined)
		{return 0}return b.likes.count};
	this.getLikes=function(){return(b.likes&&b.likes.data&&b.likes.data.length)?b.likes.data:new Array()};
	this.countComments=function()
	{if(!b.comments){return 0}if(b.comments.count==undefined){return 0}return b.comments.count};
	this.getComments=function(){return(b.comments&&b.comments.data&&b.comments.data.length)?b.comments.data:new Array()};
	this.getPostPageLink=function(){return(b.actions&&b.actions.length&&b.actions[0]&&b.actions[0].link)?b.actions[0].link:""};
	this.getPicture=function(){if(b.picture){return b.picture}
		if(b.type=="photo"&&b.object_id)
		{return"http://graph.facebook.com/"+b.object_id+"/picture/?type=album"}return null};
	var a=function(f,c)
	{try{for(var d in c){f=f.replace(c[d][0].name,'<a href="http://www.facebook.com/'+c[d][0].id+'" target="_blank">'+c[d][0].name+"</a>")}}catch(g){console.error("Error in PostParser.insertMessageTags : "+g.description?g.description:g)}return f}};
