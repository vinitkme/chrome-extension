var friendsController=new function(){
	var h=null;
	var k=false;
	var d=[];
	var j=60*60*1000;
	var f;
	var g=null;
	var e=false;
	var i=this;
	this.load=function(l){
		g=l;
		k=false;
		e=false;
		facebookAPI.fetchGraphRequest("me/friends",function(m,p,o){
			try{
				if(m&&m.data&&!p&&!o){
					h=m;
					f=setInterval(c,j);
					k=true;
					try{
						b(true,h.data);
						if(l){
							l(true)}
					}
					catch(n){
						console.error("exception occurred while doing post loading operations : "+(n.message?n.message:n))}
				}
				else{
					a("failed loading friends : "+facebookUtils.getErrorMessageFromAjaxResponse(m))}}
			catch(n){
				a("exception occurred while loading friends data : "+(n.message?n.message:n))
			}
		})};
		this.getFriendsIds=function(){
			var n=h.data.length;
			var m=new Array();
			for(var l=0;l<n;l++){
				m.push(h.data[l].id)}
			return m};
		this.register=function(l){
			d.push(l);
			if(k){
				return h.data}
			else{
				return null}
		};
		this.unregisterAll=function(){d=[]};
		this.loadFailed=function(){return e};
		this.clear=function(l){
			if(f){clearInterval(f);
				f=null}
			g=null;
			k=false;
			h=null;
			if(l)
			{d=[]}};
		this.isDataReady=function(){
			return k};
		var c=function(){
			if(!mainController.isLoggedIn())
			{return}
			if(f){
				clearInterval(f);
				f=null}
			facebookAPI.fetchGraphRequest("me/friends",function(l){
				try{
					if(!l||!l.data){
						var m=facebookUtils.getErrorMessageFromAjaxResponse(l);
						console.error("failed load friends updates : "+m);
						mainController.checkTokenValidation(m);
						if(mainController.isLoggedIn()){
							f=setInterval(c,j)}
					}
					else{
						friendsMap=new Object();
						for(var n=0;n<h.data.length;n++)
			
			{
				friendsMap[h.data[n].id]=h.data[n]}newFriends=new Array();
			for(var n=0;n<l.data.length;n++)
			{
				if(!friendsMap[l.data[n].id]){
					newFriends.unshift(l.data[n]);h.data.unshift(l.data[n])}}
			updatedFriendsMap=new Object();
			for(var n=0;n<l.data.length;n++){
				updatedFriendsMap[l.data[n].id]=l.data[n]}
			var o=new Array();
			for(var n=h.data.length-1;n>=0;n--){
				if(!updatedFriendsMap[h.data[n].id]){o.push(h.data[n]);
					h.data.splice(n,1)}}
			var p={};
			p.newFriends=newFriends;p.removedFriends=o;
			try{
				if((p.newFriends&&p.newFriends.length)||(p.removedFriends&&p.removedFriends.length)){
					b(true,p)}
			}
			catch(q){
				console.error("exception occurred while notifing to observers : "+(q.message?q.message:q))}
			f=setInterval(c,j)}
				}
				catch(q){
					console.error("error occurred when updating friends : "+(q.message?q.message:q));f=setInterval(c,j)}
			}
			)};
		var b=function(o,m){
			for(var l=0;l<d.length;l++){
				try{
					d[l].notify(o,m,"friends")
				}
				catch(n){
					console.error("error occurred when notifying : "+(n.message?n.message:n))}
			}
		};
		var a=function(l){
			console.error(l);
			e=true;b(false);
			if(g){
				g(false)
			}
			i.clear(false)
		}
}();
